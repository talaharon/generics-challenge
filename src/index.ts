// import log from "@ajar/marker";
// import { saySomething } from "./myModule.js";

//----------------------------
//      every challenge
//----------------------------
// implement the `every` function
// add types and generics as needed
const students = ["jOHn", "DAn", "ruTH", "jAnE", "beN"];

const hasN = (str:string) => str.toLowerCase().includes("n");
const min3Chars = (str:string) => str.length >= 3;

const ages = [72, 16, 22, 42, 36, 11, 52];
const over21 = (n:number) => n > 21;

//implement the `every` function
const every = <T>(items:T[], cb: (ele:T) => boolean):boolean => {
    for(let ele of items){
        if(!cb(ele)){
            return false;
        }
    }
    return true;
};

const all_students_have_n = every(students, hasN);
const all_students_min_3 = every(students, min3Chars);
const all_ages_over_21 = every(ages, over21);

console.log({ all_students_have_n }); // -> false
console.log({ all_students_min_3 }); // -> true
console.log({ all_ages_over_21 }); // -> false
